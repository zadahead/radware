require('dotenv').config();

const express = require('express');
const cors = require('cors');
const { v4 } = require('uuid');
const app = express();


const bcrypt = require('bcrypt');
const saltRounds = 10;

console.log('MYPASS => ', process.env.MYPASS);

app.use(express.json());

app.use(cors({
    origin: 'http://localhost:4000'
}))

const users = [
    
]

app.get('/me', (req, res) => {
    const userId = req.headers['user-id'];
    const user = users.find((u) => {
        return u.id === userId;
    })

    res.send(user);
})

app.post('/register', (req, res) => {
    const { username, password } = req.body;

    console.log(username, password);

   

    bcrypt.genSalt(saltRounds, function(err, salt) {
        bcrypt.hash(password, salt, function(err, hash) {
            // Store hash in your password DB.

            const id = v4();


            const user = {
                username,
                password: hash,
                id
            }

            users.push(user);

            res.send(user);
        });
    });

 
})

app.post('/login', function(req, res) {
    console.log(req.body);

    const user = users.find(function(u){
        return u.username === req.body.username;
    })

    bcrypt.compare(req.body.password, user.password, function(err, result) {
        // result == true
        if(result) {
            return res.send(user.id);
         }
         return res.status(404).send('user not found')
    });

    
})

app.get('/sayhi', function(req, res) {
    const user = {
        name: 'mosh',
        phone: '123244'
    }
    res.send('asdasdas');
});

/*
users.find(function(num){
    return num === 1
})
*/

app.get('/users', function(req, res) {
    res.send(users);
})

// ?  sort = 1  &  startwith = sa
app.get('/users/:id', function(req, res) {
    console.log('header', req.headers);
    console.log('params', req.params); //req.params.id
    console.log('quert', req.query);

    const user = users.find(function(u){
        return u.id === req.params.id;
    })

    if(user) {
        return res.send(user);
    }
    
    return res.status(404).send({ error: true, message: 'user not found'});
})

app.post('/sayhi', function(req, res) {
    res.send('you tried to create');
})

/*
CRUD

POST /users

GET /users

GET /users/:id

PUT /users

DELETE /users

*/


app.listen(5001, function() {
    console.log('restAPI is running on port 5001');
})