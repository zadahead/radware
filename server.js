const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {
    const url = req.url;
    console.log('url =>>111 ', url);

    const html = fs.readFileSync('javascript.html', 'utf-8');

    const newHtml = html.replace(/{{content}}/, url);

    res.write(newHtml);
    res.end();
})

server.listen(4000, () => {
    console.log('server is listening on port 4000');
})

